const vm = require("vm");
const funcHandler = require('../handler/default');
const transform = require("./transform");
const command = require("./command");
const logger = require('./logger')

/**
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 * @typedef {import('fastify').FastifyInstance} FastifyInstance
 *
 * @typedef {Object} FassContext
 * @property {string} requestId - requestId
 * @property {string} path - path
 * @property {string} fassPath - fassPath
 * @property {Object} headers - headers
 * @property {Object} query - query
 * @property {Object} body - body
 * @property {FastifyRequest} request - request
 * @property {FastifyReply} reply - reply
 *
 * @typedef {Object} Func
 * @property {bigint} id - id
 * @property {string} name - name
 * @property {string} path - path
 * @property {Array<string>} methods - methods
 * @property {string} code - code
 * @property {Date} updateTime - updateTime
 * 
 * @typedef {Object} Html
 * @property {bigint} id - id
 * @property {string} name - name
 * @property {string} path - path
 * @property {string} code - code
 * @property {Date} updateTime - updateTime
 *
 * @typedef {Object} Dependency
 * @property {bigint} id - id
 * @property {string} name - name
 * @property {Date} updateTime - updateTime
 * @property {bool} isDeleted - version
 * 
 * @typedef {Object} FuncExports
 * @property {string} path - path
 * @property {Array<string>} methods - methods
 * @property {Object} exports - exports
 */

/**
 * @type {Map<string,{}>} module 函数module
 */
const funcCache = new Map();

/**
 * require 
 * @param {string} id 
 * @returns 
 */
function _require(id) {
    // console.log("加载require id: " + id);
    if (id.startsWith(`./`)) {
        const path = id.substring(2, id.length)
        return funcCache.get(path)
    }
    return require(id);
}

/**
 * Create a context
 * @param {FastifyRequest} request - The Fastify request
 * @param {FastifyReply} reply - The Fastify reply object
 * @returns {FassContext} context
 */
function context(request, reply) {
    const path = request.url.split('?')[0];
    const fassPath = path.substring(1, path.length);
    return {
        path: path,
        fassPath: fassPath,
        query: request.query,
        method: request.method,
        body: request.body,
        headers: request.headers,
        requestId: request.id,
        request: request,
        reply: reply
    };
}

/**
 * compile code to exports
 * @param {string} code - code
 */
async function compileCode(code) {
    const exports = {}
    const sandbox = vm.createContext({
        exports: exports,
        require: _require,
        console: console
    });
    const script = new vm.Script(code);
    script.runInNewContext(sandbox, {});
    return exports;
}

/**
 * execute code
 * @param {FassContext} ctx - context
 */
async function handle(ctx) {
    const path = ctx.fassPath
    // auth 
    if (funcCache.has(path)) {
        /** @type {FuncExports} */
        const funcExports = funcCache.get(path);
        if (funcExports.methods.includes(ctx.method)) {
            return execute(funcExports.exports, ctx);
        }
    }
    return JSON.stringify(ctx, (key, value) => key === "request" || key === "reply" ? undefined : value);
}

/**
 * execute exports
 * @param {Object} exports - exports
 * @param {FassContext} ctx - context
 */
async function execute(exports, ctx) {
    const main = exports.default || exports.main || exports;
    return main(ctx);
}

/**
 * init
 * @param {FastifyInstance} fastify - fastify
 */
async function init(fastify) {
    // init watch dependencies
    await funcHandler.dependencyWatch((name, version) => dependencyInstall(name, version), (name) => dependencyUninstall(name))
    // init watch functions
    await funcHandler.funcPublishWatch((func) => functionRegister(func), (path) => functionUnRegister(path))
}

/**
 * register function
 * @param {Func} func - func
 */
async function functionRegister(func) {
    try {
        logger.info(`register function ${func.path} ...`)
        const transformCode = transform(func.code)
        const exports = await compileCode(transformCode)
        /** @type {FuncExports} */
        const funcExports = {
            path: func.path,
            methods: func.methods,
            exports: exports
        }
        funcCache.set(func.path, funcExports);
        logger.info(`register function ${func.path} success`)
    } catch (error) {
        logger.error(`register function ${func.path} error: ${error.message}`)
        return
    }
}

/**
 * unregister function
 * @param {string} path - path
 */
async function functionUnRegister(path) {
    logger.info(`unregister function ${path} ...`)
    funcCache.delete(path);
}

/**
 * install dependencies
 * @param {Dependency} dependency
 */
async function dependencyInstall(dependency) {
    const dependencyDesc = dependency.name + '@' + dependency.version
    logger.info(`Install ${dependencyDesc} ...`)
    const installCommand = `npm install ${dependencyDesc} ${global.npmInstallFlags}`
    return command(installCommand, (state, message) => {
        if (state) {
            logger.info(`Install ${dependencyDesc} successfully`)
        } else {
            logger.error(`Install ${dependencyDesc} error: ${message}`)
        }
    })
}

/**
 * uninstall dependencies
 * @param {string} name - name
 * @returns
 */
async function dependencyUninstall(name) {
    logger.info(`Uninstall ${name} ...`)
    const installCommand = `npm rm ${name} `
    return command(installCommand, (state, message) => {
        if (state) {
            logger.info(`Uninstall ${name} successfully`)
        } else {
            logger.error(`Uninstall ${name} error: ${message}`)
        }
    })
}

/**
 * get function
 * @param {string} path - path
 * @returns {Func} function
 */
async function funcGet(path) {
    return await funcHandler.funcGet(path)
}

/**
 * list function
 * @param {Object} query - query
 * @returns {Array<Func>} functions
 */
async function funcList(query) {
    return await funcHandler.funcList(query)
}

/**
 * save function
 * @param {Func} func - function
 * @returns {Func} function
 */
async function funcSave(func) {
    return await funcHandler.funcSave(func)
}

/**
 * delete function
 * @param {string} path - path
 * @returns {Func} function
 */
async function funcDelete(path) {
    return await funcHandler.funcDelete(path)
}

/**
 * publish function
 * @param {string} path - path
 * @returns {Func} function
 */
async function funcPublish(path) {
    return await funcHandler.funcPublish(path)
}

/**
 * unpublish function
 * @param {string} path - path
 * @returns {Func} function
 */
async function funcUnPublish(path) {
    return await funcHandler.funcUnPublish(path)
}

/**
 * list function publish
 * @param {Object} query - query
 * @returns {Array<Func>} functions
 */
async function funcPublishList(query) {
    return await funcHandler.funcPublishList(query)
}

/**
 * list dependencies
 * @returns {Array<Dependency>} dependencies
 */
async function dependencyList() {
    return await funcHandler.dependencyList()
}

/**
 * save dependency
 * @param {Dependency} dependency - dependency
 * @returns {Dependency} dependency
 */
async function dependencySave(dependency) {
    return await funcHandler.dependencySave(dependency)
}

/**
 * remove dependency
 * @param {string} name - name
 * @returns {Dependency} dependency
 */
async function dependencyRemove(name) {
    return await funcHandler.dependencyRemove(name)
}

/**
 * get function
 * @param {string} path - path
 * @returns {Func} function
 */
async function htmlGet(path) {
    return await funcHandler.htmlGet(path)
}

module.exports = {
    init,
    context,
    transform,
    compileCode,
    handle,
    execute,
    dependencyList,
    dependencySave,
    dependencyRemove,
    dependencyInstall,
    dependencyUninstall,
    funcGet,
    funcList,
    funcSave,
    funcDelete,
    funcPublish,
    funcUnPublish,
    funcPublishList,
    htmlGet
};
