const { execSync } = require('child_process');

module.exports = function (command, callback) {
    try {
        execSync(command);
        callback(true, 'Command executed successfully');
        return true;
    } catch (error) {
        callback(false, `Command executed error: ${error}`);
        return false;
    }
}
