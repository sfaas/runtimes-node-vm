const babel = require("@babel/core/lib/transform");

module.exports = function (code) {
    return babel.transformSync(code, {
        presets: ['@babel/preset-env']
    })?.code || '';
}
