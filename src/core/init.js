const command = require('./command')
const logger = require('./logger')

/**
 * init function
 * @param {FastifyInstance} fastify
 */
module.exports = async function (fastify) {
    logger.info(`initialization npm installing ...`)
    const npmInitDependencies = process.env.NPM_INIT_DEPENDENCIES
    if (npmInitDependencies && npmInitDependencies.length > 0) {
        logger.info(`npm install ${npmInitDependencies}`)
        command(`npm install ${npmInitDependencies} ${global.npmInstallFlags} --no-save`, (success, message) => {
            if (!success) {
                logger.error(`npm init installed failed: ${message}`)
                process.exit(1)
            }
        })
    }
    logger.info(`initialization npm installed success`)
}
