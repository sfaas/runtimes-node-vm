const pino = require("pino")({
    name: 'sfaas',
    level: 'info',
    messageKey: 'msg',
    // timestamp: () => `,"time":"${new Date().toISOString()}"`,
    // stream: {
    //     write: (message) => {
    //         123123
    //         const logObj = JSON.parse(message);
    //         const customLogMessage = `${new Date(logObj.time).toISOString()} [${logObj.level}] ${logObj.msg}`;
    //         console.log(customLogMessage);
    //     }
    // }
    // serializers: {
    //     res(reply) {
    //         // The default
    //         return {
    //             statusCode: reply.statusCode
    //         }
    //     },
    //     req(request) {
    //         return {
    //             method: request.method,
    //             url: request.url,
    //             path: request.routerPath,
    //             parameters: request.params,
    //             // Including the headers in the log could be in violation
    //             // of privacy laws, e.g. GDPR. You should use the "redact" option to
    //             // remove sensitive fields. It could also leak authentication data in
    //             // the logs.
    //             headers: request.headers
    //         };
    //     }
    // }
});

module.exports = pino;
