const funcContext = require('../../core/context');

/**
 * @typedef {import('../../core/context').FassContext} context
 * @typedef {import('../../core/context').Func} func
 */
module.exports = function (fastify) {
    return {
        post: {
            handler: async function (request, reply) {
                /** @type {context} */
                const ctx = funcContext.context(request, reply);
                /** @type {func} */
                const func = ctx.body
                // check compile success
                const transformCode = funcContext.transform(func.code)
                await funcContext.compileCode(transformCode)
                await funcContext.funcPublish(func);
                reply._send("publish success.")
            },
        },
    }
}
