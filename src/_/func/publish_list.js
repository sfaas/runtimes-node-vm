const funcContext = require('../../core/context');

/**
 * @typedef {import('../../core/context').FuncContext} context
 * @typedef {import('../../core/context').Func} func
 */
module.exports = function (fastify) {
    return {
        post: {
            handler: async function (request, reply) {
                /** @type {context} */
                const ctx = funcContext.context(request, reply);
                reply._send(await funcContext.funcPublishList(ctx.body))
            },
        },
    }
}
