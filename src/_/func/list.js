const funcContext = require('../../core/context');

module.exports = function (fastify) {
    return {
        get: {
            handler: async function (request, reply) {
                reply._send(await funcContext.funcList())
            },
        },
    }
}
