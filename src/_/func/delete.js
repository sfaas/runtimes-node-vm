const funcContext = require('../../core/context');

/**
 * @typedef {import('../../core/context').FuncContext} context
 * @typedef {import('../../core/context').Func} func
 */

module.exports = function (fastify) {
    return {
        delete: {
            handler: async function (request, reply) {
                /** @type {context} */
                const ctx = funcContext.context(request, reply);
                await funcContext.funcDelete(ctx.query.path)
                reply._send("Function deleted successfully.")
            },
        },
    }
}
