const funcContext = require('../../core/context');

/**
 * @typedef {import('../../core/context').FuncContext} context
 * @typedef {import('../../core/context').Func} func
 */

module.exports = function (fastify) {
    return {
        post: {
            handler: async function (request, reply) {
                /** @type {context} */
                const ctx = funcContext.context(request, reply);
                /** @type {func} */
                const func = ctx.body
                const transformedCode = funcContext.transform(func.code);
                await funcContext.funcSave(func);
                const exports = await funcContext.compileCode(transformedCode);
                reply._send(await funcContext.execute(exports, ctx))
            },
        },
    }
}
