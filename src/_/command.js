const command = require('../core/command');
const funcContext = require('../core/context');

module.exports = function (fastify) {
    return {
        post: {
            handler: function (request, reply) {
                const context = funcContext.context(request, reply)
                if (command(context.body.command)) {
                    reply._send('command execute success')
                } else {
                    reply.code(500)._send('command execute error')
                }
            },
        },
    }
}
