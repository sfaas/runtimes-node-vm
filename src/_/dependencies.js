module.exports = function (fastify) {
    return {
        get: {
            handler: function (request, reply) {
                reply._send('health')
            },
        },
    }
}
