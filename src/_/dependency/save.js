const funcContext = require('../../core/context');

module.exports = function (fastify) {
    return {
        post: {
            handler: async function (request, reply) {
                const ctx = funcContext.context(request, reply);
                await funcContext.dependencySave(ctx.body);
                reply._send("Dependency installed successfully.");
            },
        },
    }
}
