const funcContext = require('../../core/context');

module.exports = function (fastify) {
    return {
        get: {
            handler: async function (request, reply) {
                const dependencies = await funcContext.dependencyList();
                reply._send(dependencies);
            },
        },
    }
}
