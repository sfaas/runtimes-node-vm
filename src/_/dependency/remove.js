const funcContext = require('../../core/context');

module.exports = function (fastify) {
    return {
        delete: {
            handler: async function (request, reply) {
                const ctx = funcContext.context(request, reply);
                await funcContext.dependencyRemove(ctx.query.name);
                reply._send("Dependency uninstall success");
            },
        },
    }
}
