const funcContext = require('../../core/context');

/**
 * @typedef {import('../../core/context').FuncContext} context
 * @typedef {import('../../core/context').Func} func
 */
module.exports = function (fastify) {
    return {
        get: {
            handler: async function (request, reply) {
                /** @type {context} */
                const context = funcContext.context(request, reply);
                /** @type {func} */
                const func = await funcContext.htmlGet(context.query.path);
                reply._send(func)
            },
        },
    }
}
 