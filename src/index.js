const path = require("path")
const autoload = require('@fastify/autoload')
const autoRoutes = require('fastify-autoroutes')
const fastify = require('fastify')({ logger: false, pluginTimeout: 20000 });
const logger = require('./core/logger')
const config = require('./core/config')
const init = require('./core/init');
const { hostname } = require("os");

global.npmInstallFlags = process.env.NPM_INSTALL_FLAGS || '--registry=https://registry.npmmirror.com --no-save'

/**
 * @typedef {import('fastify').FastifyInstance} FastifyInstance
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 */
async function main() {
    global.fastify = fastify
    await init(fastify)

    const context = require('./core/context')
    const handler = require('./handler/default')

    logger.info('init handler ...')
    await handler.init(fastify)
    logger.info('init handler success')

    // 注册插件
    fastify.register(autoload, { dir: path.join(__dirname, 'plugins') })
    // 加载内置接口
    fastify.register(autoRoutes, { dir: '../_', prefix: config.BUILT_IN_URL_PREFIX })

    // 在所有插件注册完成后再注册依赖包
    fastify.after(() => context.init(fastify))

    // 监听所有请求
    fastify.all('*', async function handler(request, reply) {
        const ctx = context.context(request, reply);
        return context.handle(ctx)
    })

    // Error handling
    fastify.setErrorHandler(async (error, request, reply) => {
        reply.code(500)._send(error.message)
    })

    // 统一的方法返回
    fastify.decorateReply('_send', async function (payload) {
        if (this.statusCode === 200) {
            this.send({ code: 200, message: 'Success', data: payload });
        } else {
            this.send({ code: this.statusCode, message: 'Error', data: payload });
        }
    });

    // 统一的方法返回
    fastify.decorateReply('_html', async function (payload) {
        this.type('text/html');
        this.send(payload);
    });

    // 鉴权
    fastify.addHook('preValidation', async (request, reply) => {
        if (request.url.includes(config.BUILT_IN_URL_PREFIX)) {
            if (!await handler.authentication(request, reply)) {
                reply.code(403)
                reply.send({ code: 403, message: 'Unauthorized' })
            }
        }
    })

    // Unified encapsulation of return information
    // fastify.addHook('onRequest',
    //     /**
    //     * @param {FastifyReply} reply
    //     * @param {FastifyRequest} request
    //     * @param {any} payload
    //     */
    //     async (request, reply) => {
    //         if (request.url.includes(config.BUILT_IN_URL_PREFIX)) {
    //             reply.type('application/json');
    //         } else {
    //             handler.onRequest(request, reply)
    //         }
    //     }
    // );
    // Unified encapsulation of return information
    // fastify.addHook('preSerialization',
    //   /**
    //   * @param {FastifyReply} reply
    //   * @param {FastifyRequest} request
    //   * @param {any} payload
    //   */
    //   async (request, reply, payload) => {
    //     console.log('preSerialization:', request.url)
    //     if (request.url.includes(config.BUILT_IN_URL_PREFIX)) {
    //       // Determine the HTTP code
    //       if (reply.statusCode === 200) {
    //         return { code: 200, message: 'Success', data: payload }
    //       } else {
    //         return { code: reply.statusCode, message: 'Error', data: payload }
    //       }
    //     }
    //     return handler.preSerialization(request, reply, payload)
    //   }
    // );
    // Unified encapsulation of return information
    // fastify.addHook('on_send',
    //   /**
    //   * @param {FastifyReply} reply
    //   * @param {FastifyRequest} request
    //   * @param {any} payload
    //   */
    //   async (request, reply, payload) => {
    //     console.log('on_send:', payload)
    //     if (request.url.includes(config.BUILT_IN_URL_PREFIX)) {
    //       if (reply.statusCode === 200) {
    //         payload = { code: 200, message: 'Success', data: payload }
    //       } else {
    //         payload = { code: reply.statusCode, message: 'Error', data: payload }
    //       }
    //       return JSON.stringify(payload)
    //     }
    //     return handler.onsend(request, reply, payload)
    //   }
    // );

    // 启动服务
    fastify.listen({ host: "0.0.0.0", port: 3000 }, (err, address) => {
        if (err) {
            logger.error(`server start failed: ${err}`)
            process.exit(1)
        }
        logger.info(`server listening on ${address}`)
    })
}

main()
