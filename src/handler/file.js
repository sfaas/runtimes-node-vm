const logger = require('../core/logger')
const fs = require("fs");
const chokidar = require("chokidar");
const userscript = require('userscript-meta')

/**
 * @typedef {import("../core/context").Func} Func
 * @typedef {import("../core/context").Dependency} Dependency
 * @typedef {import('fastify').FastifyInstance} FastifyInstance
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 */

const filePath = process.env.FILE_PATH || 'local';

const func = `${filePath}/func`;
const publish = `${filePath}/publish`;
const dependency = `${filePath}/dependency.json`;

/**
 * init function
 * @param {FastifyInstance} fastify
 */
async function init(fastify) {
    if (!fs.existsSync(func)) fs.mkdirSync(func, { recursive: true });
    if (!fs.existsSync(publish)) fs.mkdirSync(publish, { recursive: true });
    if (!fs.existsSync(dependency)) fs.writeFileSync(dependency, `{}`, { recursive: true });
}

/**
 * watch dependency change
 * @param {Function} install - install dependency
 * @param {Function} uninstall - uninstall dependency
 */
async function dependencyWatch(install, uninstall) {

}

/**
 * watch func change
 * @param {Function} register - register function
 * @param {Function} unregister - unregister function
 */
async function funcPublishWatch(register, unregister) {
    const watcher = chokidar.watch([
        `${publish}/**/*.js`,
    ], {
        persistent: true
    })
    watcher.on('add', filename => {
        const code = fs.readFileSync(filename, 'utf-8');
        Promise.resolve(getFuncPath(filename)).then((path) => register(path, code))
    })
    watcher.on('change', filename => {
        const code = fs.readFileSync(filename, 'utf-8');
        Promise.resolve(getFuncPath(filename)).then((path) => register(path, code))
    })
    watcher.on('unlink', filename => {
        Promise.resolve(getFuncPath(filename)).then((path) => unregister(path))
    })
}

async function getFuncPath(path) {
    console.log(path)
    var name = path.replace(`\\`, '/').replace('.js', '')
    console.log(name)
    return name
}

/**
 * @returns {FindCursor<Dependency>} dependencies
 */
async function dependencyList() {
    return []
}

/**
 * @param {Dependency} dependency
 */
async function dependencySave(dependency) {
    return null
}

/**
 * @param {string} name
 */
async function dependencyRemove(name) {
    return;
}

/**
 * Perform the funcGet operation with the provided context
 * @param {string} path - The context object containing request and reply information
 */
async function funcGet(path) {
    return
}

/**
 * Perform the funcList operation with the provided context
 * @returns {Array<Func>} The context object containing request and reply information
 */
async function funcList() {

}

/**
 * @param {Func} func - func
 * @returns {Func}
 */
async function funcSave(func) {
    const fileName = func.path + ".js"
    console.log(fileName)
    userscript.stringify({
        ...func
    })
    console.log(func)
}

/**
 * @param {string} path - path
 */
async function funcDelete(path) {

}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublish(func) {

}

/**
 * publish delete
 * @param {string} path - func path
 */
async function funcUnPublish(path) {

}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublishList(func) {

}

module.exports = {
    init,
    dependencyWatch,
    dependencyList,
    dependencySave,
    dependencyRemove,
    funcGet,
    funcList,
    funcSave,
    funcDelete,
    funcPublish,
    funcUnPublish,
    funcPublishList,
    funcPublishWatch
};
