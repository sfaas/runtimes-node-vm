const logger = require('../core/logger')
const { MongoClient, ObjectId } = require('mongodb')

/**
 * @typedef {import("../core/context").Func} Func
 * @typedef {import("../core/context").Dependency} Dependency
 * @typedef {import('mongodb').FindCursor} FindCursor
 * @typedef {import('mongodb').ObjectId} ObjectId
 * @typedef {import('mongodb').MongoClient} MongoClient
 * @typedef {import('fastify').FastifyInstance} FastifyInstance
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 */

const uri = process.env.MONGO_URI || 'mongodb://localhost:27017';
const dbName = process.env.MONGO_DB || 'sfaas';
const client = new MongoClient(uri, {});
const db = client.db(dbName);

const funcCollection = () => db.collection(process.env.MONGO_FUNC_COLLECTION || 'func');
const htmlCollection = () => db.collection(process.env.MONGO_HTML_COLLECTION || 'html');
const publishCollection = () => db.collection(process.env.MONGO_PUBLISH_COLLECTION || 'publish');
const dependencyCollection = () => db.collection(process.env.MONGO_DEPLOYMENT_COLLECTION || 'dependency');

/**
 * init function
 * @param {FastifyInstance} fastify
 */
async function init(fastify) {
    // init mongodb
    await client.connect().then(() => {
        logger.info('Connected to MongoDB successfully!');
        fastify.decorate('mongo', { client, ObjectId });
        fastify.addHook('onClose', () => client.close());
    }).catch(err => {
        logger.error(`Connect MongoDB failed: ${err}`);
        process.exit(1);
    })
}

/**
 * init function
 * @param {FastifyRequest} request
 * @param {FastifyReply} reply
 */
async function authentication(request, reply) {
    return request.headers.token === process.env.TOKEN
}

/**
 * watch dependency change
 * @param {Function<Dependency>} install - install dependency
 * @param {Function} uninstall - uninstall dependency
 */
async function dependencyWatch(install, uninstall) {
    // install dependencies
    /** @type {FindCursor<Dependency>} */
    const dependencies = await dependencyList()
    dependencies.forEach(install);

    const watch = dependencyCollection().watch()
    watch.on("change", async (next) => {
        const operationType = next.operationType
        if (operationType === "insert") {
            /** @type {Dependency} */
            const dependency = next.fullDocument
            install(dependency.name, dependency.version)
        } else if (operationType === "update") {
            const updatedFields = next.updateDescription.updatedFields
            // 变更了状态 在从数据库查询
            if (updatedFields.isDeleted !== undefined && updatedFields.isDeleted === true) {
                /** @type {Dependency} */
                const dependency = await dependencyCollection().findOne({ _id: next.documentKey._id })
                uninstall(dependency.name)
            }
        }
    });
}

/**
 * watch func change
 * @param {Function<string,string>} register - register function
 * @param {Function<string>} unregister - unregister function
 */
async function funcPublishWatch(register, unregister) {
    // install functions
    /** @type {FindCursor<any>} */
    const functions = await funcPublishList({ status: 1 })
    functions.forEach(register);

    const watch = publishCollection().watch()
    watch.on("change", async (next) => {
        const operationType = next.operationType
        if (operationType === "insert") {
            register(next.fullDocument)
        } else if (operationType === "update") {
            const updatedFields = next.updateDescription.updatedFields
            // 变更了状态 在从数据库查询
            if (updatedFields.status !== undefined && updatedFields.status === 0) {
                /** @type {Func} */
                const func = await publishCollection().findOne({ _id: next.documentKey._id })
                unregister(func.path)
            }
        }
    });
}
/**

 * @returns {FindCursor<Dependency>} dependencies
 */
async function dependencyList() {
    return dependencyCollection().find({ isDeleted: false }).toArray();
}

/**
 * @param {Dependency} dependency
 */
async function dependencySave(dependency) {
    logger.info(`handler => dependencySave param => name:${dependency.name} version:${dependency.version}`)
    const exist = await dependencyCollection().findOne({ name: dependency.name, isDeleted: false })
    if (exist) throw new Error(`Dependency ${dependency.name}@${dependency.version} already installed.`)
    dependency.isDeleted = false;
    dependency.updateTime = new Date()
    return dependencyCollection().insertOne(dependency);
}

/**
 * @param {string} name
 */
async function dependencyRemove(name) {
    const filter = { name: name, isDeleted: false }
    logger.info(`handler => dependencyRemove param => name:${name}`)
    const exist = await dependencyCollection().findOne(filter)
    if (!exist) throw new Error(`Dependency ${name} not found.`)
    return dependencyCollection().updateOne(filter, { $set: { isDeleted: true, updateTime: new Date() } });
}

/**
 * Perform the funcGet operation with the provided context
 * @param {string} path - The context object containing request and reply information
 */
async function funcGet(path) {
    logger.info(`handler => funcGet param => path:${path}`)
    const func = await funcCollection().findOne({ path: path });
    if (!func) throw new Error(`Function ${path} not found.`)
    return convertObject(func);
}

/**
 * Perform the funcList operation with the provided context
 * @returns {Array<Func>} The context object containing request and reply information
 */
async function funcList() {
    return funcCollection().find().map(func => convertObject(func)).toArray();
}

/**
 * @param {Func} func - func
 * @returns {Func}
 */
async function funcSave(func) {
    logger.info(`handler => funcSave param => path:${func.path}`)
    const _id = new ObjectId(func.id);
    delete func.id;
    func.updateTime = new Date()
    return funcCollection().updateOne({ _id: _id }, { $set: func }, { upsert: true });
}

/**
 * @param {string} path - path
 */
async function funcDelete(path) {
    /** @type {Func} */
    // const func = await funcGet(path)
    await funcCollection().deleteOne({ path: path });
    await publishCollection().deleteMany({ path: path });
}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublish(func) {
    delete func.id;
    func.status = 1
    func.updateTime = new Date()
    await funcUnPublish(func.path);
    logger.info(`handler => funcPublish param => path:${func.path}`)
    return publishCollection().insertOne(func);
}

/**
 * publish delete
 * @param {string} path - func path
 */
async function funcUnPublish(path) {
    logger.info(`handler => funcUnPublish param => path:${path}`)
    return publishCollection().updateMany({ path: path }, { $set: { status: 0 } });
}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublishList(func) {
    return publishCollection()
        .find(func)
        .sort({ updateTime: -1 })
        // .skip((page - 1) * size)
        // .limit(size)
        .map(func => convertObject(func))
        .toArray();
}

/**
 * Perform the funcGet operation with the provided context
 * @param {string} path - The context object containing request and reply information
 */
async function htmlGet(path) {
    logger.info(`handler => htmlGet param => path:${path}`)
    const html = await htmlCollection().findOne({ path: path });
    if (!html) throw new Error(`Html ${path} not found.`)
    return html.code;
}

/**
 *
 * @param {Func} func - func
 * @returns {Func}
 */
function convertObject(func) {
    func.id = func._id;
    delete func._id;
    return func;
}

module.exports = {
    init,
    authentication,
    dependencyWatch,
    dependencyList,
    dependencySave,
    dependencyRemove,
    funcGet,
    funcList,
    funcSave,
    funcDelete,
    funcPublish,
    funcUnPublish,
    funcPublishList,
    funcPublishWatch,
    htmlGet
};
