const logger = require('../core/logger')
const fs = require('fs')
/**
 * @typedef {import("../core/context").Func} Func
 * @typedef {import("../core/context").Dependency} Dependency
 * @typedef {import('fastify').FastifyInstance} FastifyInstance
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 */

/**
 * init function
 * @param {FastifyInstance} fastify
 */
async function init(fastify) {
 
}

/**
 * watch dependency change
 * @param {Function} install - install dependency
 * @param {Function} uninstall - uninstall dependency
 */
async function dependencyWatch(install, uninstall) {

}

/**
 * watch func change
 * @param {Function} register - register function
 * @param {Function} unregister - unregister function
 */
async function funcWatch(register, unregister) {

}

/**
 * @returns {FindCursor<Dependency>} dependencies
 */
async function dependencyList() {
    return []
}

/**
 * @param {Dependency} dependency
 */
async function dependencySave(dependency) {
    return null
}

/**
 * @param {string} name
 */
async function dependencyRemove(name) {
    return;
}

/**
 * Perform the funcGet operation with the provided context
 * @param {string} path - The context object containing request and reply information
 */
async function funcGet(path) {
    return
}

/**
 * Perform the funcList operation with the provided context
 * @returns {Array<Func>} The context object containing request and reply information
 */
async function funcList() {

}

/**
 * @param {Func} func - func
 * @returns {Func}
 */
async function funcSave(func) {

}

/**
 * @param {string} path - path
 */
async function funcDelete(path) {

}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublish(func) {

}

/**
 * publish delete
 * @param {string} path - func path
 */
async function funcUnPublish(path) {

}

/**
 * publish save
 * @param {Func} func - func
 */
async function funcPublishList(func) {

}

module.exports = {
    init,
    dependencyWatch,
    dependencyList,
    dependencySave,
    dependencyRemove,
    funcWatch,
    funcGet,
    funcList,
    funcSave,
    funcDelete,
    funcPublish,
    funcUnPublish,
    funcPublishList,
};
