/**
 * @typedef {import('fastify').FastifyRequest} FastifyRequest
 * @typedef {import('fastify').FastifyReply} FastifyReply
 * 
 * @typedef {Object} FuncContext
 * @property {string} requestId - requestId
 * @property {string} path - path
 * @property {string} funcName - funcName
 * @property {Object} headers - headers
 * @property {Object} query - query
 * @property {Object} body - body
 * @property {FastifyRequest} request - request
 * @property {FastifyReply} reply - reply
 * 
 * @param {FuncContext} ctx 
 * @returns {Object} 
 */
export default async function (ctx) {
    return ctx.query;
};
