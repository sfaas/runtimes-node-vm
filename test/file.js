const fs = require("fs");
const vm = require("vm");
const chokidar = require("chokidar");
const babel = require("@babel/core/lib/transform");

const func = process.env.FILE_FUNC_DIR || `local\\func`;
const publish = process.env.FILE_PUBLISH_DIR || `local\\publish`;
const deployment = process.env.FILE_DEPLOYMENT_FILE || `local/deployment.json`;

if (!fs.existsSync(func)) fs.mkdirSync(func);
if (!fs.existsSync(publish)) fs.mkdirSync(publish);
if (!fs.existsSync(deployment)) fs.writeFileSync(deployment, `{}`);

const watcher = chokidar.watch([
    `${func}/**/*.js`,
    `${publish}/**/*.js`,
    `${deployment}`
], {
    persistent: true
})

watcher.on("add", filename => deal(filename));
watcher.on("change", filename => deal(filename));
watcher.on("unlink", filename => console.log(`File ${filename} has been removed`));

function deal(filename) {
    if (filename.startsWith(func) && filename.endsWith('.js')) {
        try {
            const code = fs.readFileSync(filename, 'utf-8');
            const result = transformCode(code);
            const exports = compileCode(result);
            Promise.resolve(execute(exports, {})).then((value) => {
                console.log(value);
            })
        } catch (err) {
            console.error(err);
            return;
        }
    }
}

function transformCode(code) {
    return babel.transformSync(code, {
        presets: ['@babel/preset-env']
    })?.code || '';
}

function compileCode(code) {
    const exports = {}
    const sandbox = vm.createContext({
        exports: exports,
        console: console
    });
    const script = new vm.Script(code);
    script.runInNewContext(sandbox, {});
    return exports;
}

function execute(exports, ctx) {
    const main = exports.default || exports.main || exports;
    return main(ctx);
}
