FROM node:22-alpine
WORKDIR /app
COPY . .
EXPOSE 3000
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "/app/entrypoint.sh" ]
